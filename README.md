<!-- SPDX-FileCopyrightText: Public domain -->
<!-- SPDX-License-Identifier: CC0-1.0 -->

Optional fonts for use with [@techandsoftware/teletext](https://www.npmjs.com/package/@techandsoftware/teletext)

I was not the author of the fonts. They were originally:

* Unscii by Viznut - http://viznut.fi/unscii/
* Bedstead by bjh21 - https://bjh21.me.uk/bedstead/

Unscii is required only when `teletext.setView('classic__font-for-mosaic')` is called. Put the Unscii files in a subdirectory relative to the page containing the teletext div. See the README in @techandsoftware/teletext
